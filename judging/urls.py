"""judging URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from django.conf.urls import url
from django.views import *

urlpatterns = [
    path('',include('login.urls')),
    path('admin/', admin.site.urls),
    path('login/',include('login.urls')),
    path('getNewScoreCard/',include('newscore.urls')),
<<<<<<< HEAD
    #url(r'^judgeinfo/$' , judge_info),
    path('judgeinfo/', include('judge_info.urls'))
=======
    path('addJudge/',include('addjudge.urls'))
>>>>>>> a5322584c488f32680a317194727141412a67cb9
]
