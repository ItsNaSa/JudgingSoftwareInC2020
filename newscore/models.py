from django.db import models

# Create your models here.

class scoreCard(models.Model):
    judgeID = models.CharField(max_length = 5)
    groupID = models.CharField(max_length = 10)
    totalScore = models.IntegerField()
    percentileScore = models.FloatField()

