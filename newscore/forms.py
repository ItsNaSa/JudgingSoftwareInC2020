from django import forms
from .models import scoreCard

class newScoreCardForm(forms.ModelForm):
    class Meta:
        model = scoreCard
        fields = ['groupID','totalScore','percentileScore']
    

# Modify the field names, add validations

class getJudge(forms.Form):
    judgeID = forms.CharField(label = 'Enter judge ID',max_length = 10)
    no_of_projects = forms.IntegerField(label = 'Enter the no. of projects evaluated by judge')

    