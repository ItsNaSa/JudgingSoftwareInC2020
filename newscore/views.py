from django.shortcuts import render,redirect
from django.http import HttpResponse,HttpResponseRedirect
from .models import scoreCard
from .forms import newScoreCardForm,getJudge
from addjudge.models import newJudge
# Create your views here.

no_of_judged_projects = 0

def enterJudgeInfo(request):
	if request.method == 'POST':
		# if request.session['name'] is None:
			# return to the login page
		# else:	
			# else we got the info of judge
			judgeInfo = getJudge(request.POST)
			if judgeInfo.is_valid():
				tempJudgeID = judgeInfo['judgeID'].value()
				no_of_judged_projects = judgeInfo['no_of_projects'].value()
				
				# if newJudge.objects.get(judgeID = tempJudgeID):
				request.session['judgeID'] = tempJudgeID
				request.session['no_of_projects'] = no_of_judged_projects
				# go for adding new score card
				return HttpResponseRedirect('addNewScoringValues')
				# else: # go back and say check ID again
			# then go on to add those project's score card
	else:
		who_judged_the_projects = getJudge()
	return render(request,'who_judged.html',{'newCard':who_judged_the_projects})

def addNewScoreCard(request):
	if request.method == 'POST':
		# check if user is logged in or not
		# if request.session['name'] is None:
		# 	HttpResponseRedirect('')    # go to the login app 
		# else: # add the score
			newScoreCard = newScoreCardForm(request.POST)   
			if newScoreCardForm.is_valid():
				return 0
	else:
		newScoreCard = newScoreCardForm()

	return render(request,'scoreCard.html',{'newCard':newScoreCard,'no_of_judged_projects':no_of_judged_projects}) 