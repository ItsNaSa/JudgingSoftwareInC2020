from django.urls import path
from . import views

urlpatterns = [
    path('',views.enterJudgeInfo,name='who_judged'),
    # path('getJudge',views.enterJudgeInfo,name='who_judged'),
    path('addNewScoringValues',views.addNewScoreCard,name='new_score_card'),
]