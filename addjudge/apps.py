from django.apps import AppConfig


class AddjudgeConfig(AppConfig):
    name = 'addjudge'
