from django import forms
from django.core.validators import RegexValidator
mobile_regex = RegexValidator(regex=r'^[0-9]{10}$|^[0-9]{12}$', message="Phone number must not consist of space and country code. eg : 6591258565")

class addJudgeForm(forms.Form):
	judgeName = forms.CharField(label = "Judge Name",max_length=50)
	emailID = forms.EmailField(label = "Email ID",max_length=50)
	phoneNo = forms.CharField(max_length=10,validators=[mobile_regex])
	companyName = forms.CharField(label = "Company Name",max_length=50)
	designation = forms.CharField(label = "Designtion",max_length=50)