from django.http import HttpResponse,HttpResponseRedirect
from django.shortcuts import render
from .models import newJudge
from .forms import addJudgeForm

# Create your views here.
def add_Judge(request):
	if request.method == 'POST':
		JudgeForm = addJudgeForm(request.POST)
		if JudgeForm.is_valid():
			Judge = newJudge()
			Judge.judgeName = JudgeForm['judgeName'].value()
			Judge.emailID = JudgeForm['emailID'].value()
			Judge.phoneNo = JudgeForm['phoneNo'].value()
			Judge.companyName = JudgeForm['companyName'].value()
			Judge.designation = JudgeForm['designation'].value()

			Judge.save()
			return HttpResponse("Added Successfully")
	else:
		JudgeForm = addJudgeForm()

	return render(request,'addJudge.html',{'addJudgeForm':JudgeForm})