from django.db import models

# Create your models here.
class newJudge(models.Model):
	judgeID = models.AutoField(primary_key = True)
	judgeName = models.CharField(max_length = 50)
	emailID = models.EmailField()
	phoneNo = models.BigIntegerField()
	companyName = models.CharField(max_length = 20)
	designation = models.CharField(max_length = 20)

	