from django.shortcuts import render
from .forms import *
from django.http import *

def judge_info(request):
    if request.method == 'POST':
        form = judge_fill(request.POST)           #create the object of form 
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/judge_info/')
    else:
        form = judge_fill()
    return render(request , 'fill.html' , {'form':form})    
