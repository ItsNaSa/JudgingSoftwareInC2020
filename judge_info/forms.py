from django import forms
from .models import *
from django.forms import ModelForm
from django.db import models


class judge_fill(forms.Form):
    judgename    = forms.CharField(widget=forms.TextInput() , required=True , max_length=100)
    company      = forms.CharField(widget=forms.TextInput() , required=True , max_length=100)
    designation  = forms.CharField(widget=forms.TextInput() , required=True , max_length=50)
    #email        = forms.EmailField(widget=forms.EmailField() , required=True , max_length=100)
    email        = forms.CharField(widget=forms.TextInput() , required=True , max_length=50)
    mobile       = forms.CharField(widget=forms.TextInput() , required=True , max_length=100)
    judge_id     = forms.CharField(widget=forms.TextInput() , required=True , max_length=100)

class Meta:
    model = judg1
    fields = ['judge_name' , 'company' , 'designation' , 'email_id' , 'mobile_no' , 'judge_id']    