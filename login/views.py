from django.http import HttpResponse,HttpResponseRedirect
from django.shortcuts import render
from .models import user
from .forms import loginForm
# Create your views here.
# When calling '/index', login page (login function) should be shown
# name = new_login
def login(request):
	if request.method == 'POST':
		newForm = loginForm(request.POST)    # create a form instance and populate it with data from the request:
		if newForm.is_valid():
			uname = newForm['username'].value()
			pasw = newForm['password'].value()
			temp1 = user.objects.get(username=uname)
			# print(temp1)
			if(temp1.password==pasw):
				request.session['name'] = uname
				return HttpResponse("DONE!!"+request.session.get('name'))
			return HttpResponseRedirect('') # Enter the app for adding new judging info
	else:
		newForm = loginForm()

	return render(request,'login.html',{'loginForm':newForm})


# submitted form will come here
# name = check_credentials
def loginattempt(request):
	return '0'


def new(request):
	return HttpResponse('You are in for a surprise!!')