from django.contrib import admin
from .models import user


# ModelAdmin provides allthe functionality to control admin
class loginAdmin(admin.ModelAdmin):
    list_display = ('username','password')


# importing user as the model name is user
admin.site.register(user,loginAdmin)